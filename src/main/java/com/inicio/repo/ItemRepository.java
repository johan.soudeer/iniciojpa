package com.inicio.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inicio.model.Item;

public interface ItemRepository extends JpaRepository<Item, Integer>{
	
	public List<Item> findByName(String n);
	
	public List<Item> findById(int id);

}
