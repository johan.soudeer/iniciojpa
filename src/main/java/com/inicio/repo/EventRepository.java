package com.inicio.repo;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.inicio.model.Event;





public interface EventRepository extends JpaRepository<Event, Integer>{
	
	public List<Event> findByTitle(String t);
    
	public Page<Event> findByTitle(String t, Pageable pageable);
		
	public Page<Event> findAll(Pageable pageable);
	
	public List<Event> findAll();
	
	public Event findById(int id);

	


}
