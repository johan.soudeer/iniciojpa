package com.inicio.repo;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.inicio.model.User;



public interface UserRepository extends JpaRepository<User, Integer>{
	
	public List<User> findByLogin(String l);
    
	public Page<User> findByLogin(String l, Pageable pageable);
		
	public User findByLoginAndPass(String l, String p);
	
	public Page<User> findAll(Pageable pageable);
	
	public List<User> findAll();
	
	public User findById(int id);

	


}
