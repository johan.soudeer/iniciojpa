package com.inicio.repo;


import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.inicio.model.Guest;




public interface GuestRepository extends JpaRepository<Guest, Integer>{
	
	public List<Guest> findByName(String n);
    
	public Page<Guest> findByName(String n, Pageable pageable);
		
	public Page<Guest> findAll(Pageable pageable);
	
	public List<Guest> findAll();
	
	public Guest findById(int id);

	


}
