package com.inicio.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.inicio.model.Address;

public interface AddressRepository extends JpaRepository<Address, Integer>{

	public List<Address> findByName(String n);
	
	public List<Address> findByStreet(String n);
	
	public List<Address> findByCity(String n);
	
	public List<Address> findByZipCode(String n);
	
	
}
