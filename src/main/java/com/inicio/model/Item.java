package com.inicio.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="item")
public class Item {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String name;
	private int neededQuantity;
	private int currentQUantity;
	
	@ManyToOne 
	@JoinColumn(name="event_FK")
	private Event event;
	
	
	public Item() {
	}
	

	public Item(String name, int neededQuantity, int currentQUantity) {
		this.name = name;
		this.neededQuantity = neededQuantity;
		this.currentQUantity = currentQUantity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNeededQuantity() {
		return neededQuantity;
	}

	public void setNeededQuantity(int neededQuantity) {
		this.neededQuantity = neededQuantity;
	}

	public int getCurrentQUantity() {
		return currentQUantity;
	}

	public void setCurrentQUantity(int currentQUantity) {
		this.currentQUantity = currentQUantity;
	}

	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
	
	
}
