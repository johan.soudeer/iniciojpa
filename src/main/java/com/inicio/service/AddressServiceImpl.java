package com.inicio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inicio.model.Address;
import com.inicio.repo.AddressRepository;

@Service
public class AddressServiceImpl implements AddressService{
	
	@Autowired
	AddressRepository addressRepository;

	@Override
	public List<Address> findByName(String n) {
		return this.addressRepository.findByName(n);
	}

	@Override
	public List<Address> findByStreet(String n) {
		return this.addressRepository.findByStreet(n);
	}

	@Override
	public List<Address> findByCity(String n) {
		return this.addressRepository.findByCity(n);
	}

	@Override
	public List<Address> findByZipCode(String n) {
		return this.addressRepository.findByZipCode(n);
	}

	@Override
	public void addAddress(Address address) {
		this.addressRepository.save(address);
		
		
	}

	@Override
	public void updateAddress(Address address) {
		this.addressRepository.save(address);
	}

	@Override
	public void deleteAddress(int id) {
		this.addressRepository.deleteById(id);	
	}

}
