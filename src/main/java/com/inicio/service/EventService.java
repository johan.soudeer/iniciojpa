package com.inicio.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.inicio.model.Event;
import com.inicio.model.Guest;




public interface EventService {
	
	public List<Event> findByTitle(String t);
    
	public Page<Event> findByTitle(String t, Pageable pageable);
		
	public Page<Event> findAll(Pageable pageable);
	
	public List<Event> findAll();
	
	public Event findById(int id);
	
	
	public void addEvent(Event ev);
	
	public void updateEvent(Event ev);
	
	public void deleteEvent(int id);
	

}
