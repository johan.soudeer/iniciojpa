package com.inicio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.inicio.model.Guest;
import com.inicio.repo.GuestRepository;




@Service
public class GuestServiceImpl implements GuestService{

	
	@Autowired
	GuestRepository repository;
	
	
	@Override
	public List<Guest> findAll() {
		return this.repository.findAll();
	}

	@Override
	public void addGuest(Guest emp) {
		 this.repository.save(emp);		
	}

	@Override
	public void updateGuest(Guest emp) {
		 this.repository.save(emp);
		
	}

	@Override
	public void deleteGuest(int id) {
		this.repository.deleteById(id);
		
	}

	@Override
	public Guest findById(int id) {
		return this.repository.findById(id);
	}


	

	@Override
	public Page<Guest> findAll(Pageable pageable) {
		return this.repository.findAll(pageable);
	}

	@Override
	public List<Guest> findByName(String n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Guest> findByName(String n, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	





}
