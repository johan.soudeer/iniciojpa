package com.inicio.service;

import java.util.List;

import com.inicio.model.Item;

public interface ItemService {
	
	public List<Item> findByName(String n);
	
	public List<Item> findById(int id);
	
	public void addItem(Item item);
	
	public void updateItem(Item item);
	
	public void deleteItem(int id);
	
}
