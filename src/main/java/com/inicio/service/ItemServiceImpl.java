package com.inicio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.inicio.model.Item;
import com.inicio.repo.ItemRepository;

@Service
public class ItemServiceImpl implements ItemService {
	
	@Autowired
	ItemRepository itemRepository;

	@Override
	public List<Item> findByName(String n) {
		return this.itemRepository.findByName(n);
	}

	@Override
	public List<Item> findById(int id) {
		return this.itemRepository.findById(id);
	}

	@Override
	public void addItem(Item item) {
		this.itemRepository.save(item);
	}

	@Override
	public void updateItem(Item item) {
	}

	@Override
	public void deleteItem(int id) {
		this.itemRepository.deleteById(id);
	}

}
