package com.inicio.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.inicio.model.Guest;



public interface GuestService {
	
	public List<Guest> findByName(String n);
    
	public Page<Guest> findByName(String n, Pageable pageable);
		
	public Page<Guest> findAll(Pageable pageable);
	
	public List<Guest> findAll();
	
	public Guest findById(int id);
	
	public void addGuest(Guest emp);
	
	public void updateGuest(Guest emp);
	
	public void deleteGuest(int id);

}
