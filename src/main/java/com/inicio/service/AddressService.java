package com.inicio.service;

import java.util.List;

import com.inicio.model.Address;

public interface AddressService {
	
	public List<Address> findByName(String n);
	
	public List<Address> findByStreet(String n);
	
	public List<Address> findByCity(String n);
	
	public List<Address> findByZipCode(String n);
	
	public void addAddress(Address address);
	
	public void updateAddress(Address address);
	
	public void deleteAddress(int id);

}
