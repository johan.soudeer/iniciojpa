package com.inicio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.inicio.model.Event;
import com.inicio.repo.EventRepository;




@Service
public class EventServiceImpl implements EventService{

	
	@Autowired
	EventRepository repository;
	
	
	@Override
	public List<Event> findAll() {
		return this.repository.findAll();
	}

	@Override
	public void addEvent(Event ev) {
		 this.repository.save(ev);		
	}

	@Override
	public void updateEvent(Event ev) {
		 this.repository.save(ev);
		
	}

	@Override
	public void deleteEvent(int id) {
		this.repository.deleteById(id);
		
	}

	@Override
	public Event findById(int id) {
		return this.repository.findById(id);
	}

	@Override
	public Page<Event> findAll(Pageable pageable) {
		return this.repository.findAll(pageable);
	}

	@Override
	public List<Event> findByTitle(String t) {
		// TODO Auto-generated method stub
		return this.repository.findByTitle(t);
	}

	@Override
	public Page<Event> findByTitle(String t, Pageable pageable) {
		// TODO Auto-generated method stub
		return this.repository.findByTitle(t, pageable);
	}



}
