package com.inicio.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.inicio.model.User;
import com.inicio.repo.UserRepository;



@Service
public class UserServiceImpl implements UserService{

	
	@Autowired
	UserRepository repository;
	
	
	@Override
	public List<User> findAll() {
		return this.repository.findAll();
	}

	@Override
	public void addUser(User emp) {
		 this.repository.save(emp);		
	}

	@Override
	public void updateUser(User emp) {
		 this.repository.save(emp);
		
	}

	@Override
	public void deleteUser(int id) {
		this.repository.deleteById(id);
		
	}

	@Override
	public User findById(int id) {
		return this.repository.findById(id);
	}

	@Override
	public List<User> findByLogin(String l) {
		return this.repository.findByLogin(l);
	}

	@Override
	public Page<User> findByLogin(String l, Pageable pageable) {
		return this.repository.findByLogin(l, pageable);
	}

	@Override
	public User findByLoginAndPass(String l, String p) {
		return this.repository.findByLoginAndPass(l, p);

	}

	@Override
	public Page<User> findAll(Pageable pageable) {
		return this.repository.findAll(pageable);
	}



}
