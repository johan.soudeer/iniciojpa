package com.inicio.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.inicio.model.User;


public interface UserService {
	
    
	public List<User> findByLogin(String l);
	
	public Page<User> findByLogin(String l, Pageable pageable);
		
	public User findByLoginAndPass(String l, String p);
	
	public Page<User> findAll(Pageable pageable);
	
	public List<User> findAll();
	
	public User findById(int id);
	
	
	public void addUser(User emp);
	
	public void updateUser(User emp);
	
	public void deleteUser(int id);


}
