package com.inicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.inicio.model.Guest;
import com.inicio.service.GuestService;




@Controller
@RequestMapping(value="/guest")
public class GuestController {
	
	@Autowired
	GuestService guestService;
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ModelAndView list()
	{
	ModelAndView model = new ModelAndView("guest/guest_page");
	List<Guest> listGuest = this.guestService.findAll();
	model.addObject("listGuest", listGuest);
	return model;
	
	}
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public ModelAndView add()
	{
	ModelAndView model = new ModelAndView("guest/guest_form");
	Guest guest = new Guest();
	model.addObject("guestForm", guest);
	return model;
	}
	
	
	
	@RequestMapping(value="/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("id") int id)
	{
	ModelAndView model = new ModelAndView("guest/guest_form");
	Guest guest = guestService.findById(id);
	model.addObject("guestForm", guest);
	return model;
	}
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("guestForm") Guest emp)
	{
		if (emp != null && emp.getId() !=0)
			this.guestService.updateGuest(emp);
		else
			this.guestService.addGuest(emp);
		return new ModelAndView("redirect:/guest/list");
	}


	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") int id)
	{
		guestService.deleteGuest(id);
	return new ModelAndView("redirect:/guest/list");
	}
	
	
}
