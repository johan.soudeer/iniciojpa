package com.inicio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.inicio.model.Address;
import com.inicio.model.Event;
import com.inicio.model.Item;
import com.inicio.service.AddressService;
import com.inicio.service.EventService;
import com.inicio.service.ItemService;


@Controller
@RequestMapping(value="/event")
public class EventController {
	
	@Autowired
	EventService EventService;
	
	@Autowired
	AddressService addressService;
	
	@Autowired
	ItemService itemService;
	
	
	@RequestMapping(value="/list", method = RequestMethod.GET)
	public ModelAndView list()
	{
	ModelAndView model = new ModelAndView("event/event_page");
	List<Event> listEvent = this.EventService.findAll();
	model.addObject("listEvent", listEvent);
	return model;
	
	}
	
	@RequestMapping(value="/add", method = RequestMethod.GET)
	public ModelAndView add()
	{
	ModelAndView model = new ModelAndView("event/event_form");
	Event event = new Event();
	Address adresse=new Address();
	model.addObject("event", event);
	model.addObject("adresse", adresse);
	return model;
	}
	
	@RequestMapping(value="/item/{id}", method = RequestMethod.GET)
	public ModelAndView upitemdate(@PathVariable("id") int id)
	{
	ModelAndView model = new ModelAndView("event/item_form");
	Item item=new Item();
	Event event=this.EventService.findById(id);
	item.setEvent(event);
	//model.addObject("event", event);
	model.addObject("item", item);
	System.out.println("Event id : " + item.getEvent().getId());

	return model;
	
	}
	
	@RequestMapping(value="/saveItem", method = RequestMethod.POST)
	public ModelAndView saveItem(
			@ModelAttribute("item") Item item,
			@ModelAttribute("item.event.id") String id
			){
		
		
		System.out.println("Event id : " + id);
/*
		Event nouvelEvent = this.EventService.findById(id);
		item.setEvent(nouvelEvent);
		List<Item> items = event.getItems();
		
		items.add(item);
		for (Item item2 : event.getItems()) {
		
			System.out.println("Nouvel Item : " + item2.getName());
		}
		this.EventService.addEvent(nouvelEvent);
		*/
		//this.itemService.addItem(item);
		return new ModelAndView("redirect:/event/list");
	}
	
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public ModelAndView save(
			@ModelAttribute("event") Event event, 
			@ModelAttribute("adresse") Address address) {
		this.addressService.addAddress(address);
		event.setAddress(address);
		this.EventService.addEvent(event);
		return new ModelAndView("redirect:/event/list");
	}
	
	
	/*
	@RequestMapping(value="/update/{id}", method = RequestMethod.GET)
	public ModelAndView update(@PathVariable("id") int id)
	{
	ModelAndView model = new ModelAndView("event/event_form");
	User user = userService.findById(id);
	model.addObject("userForm", user);
	return model;
	}
	
	@RequestMapping(value="/save", method = RequestMethod.POST)
	public ModelAndView save(@ModelAttribute("userForm") User emp)
	{
		if (emp != null && emp.getId() !=0)
			this.userService.updateUser(emp);
		else
			this.userService.addUser(emp);
		return new ModelAndView("redirect:/user/list");
	}

	
	@RequestMapping(value="/delete/{id}", method = RequestMethod.GET)
	public ModelAndView delete(@PathVariable("id") int id)
	{
		userService.deleteUser(id);
	return new ModelAndView("redirect:/user/list");
	}*/
	

	
}
