<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/bootstrap.css" rel="stylesheet"/>
<title>Event page</title>
</head>
<body>
    <spring:url value="/event/add" var="addURL" />
    <a href="${addURL}">Add Event</a>
   
    
    <h1> event list </h1>
    <table width="100%" border="1">
        <tr>
            <th>Titre</th>
            <th>Description</th>
            <th>Date Début</th>
            <th>Adresse</th>
            <th colspan="2">Actions</th>
        </tr>
        <c:forEach items="${listEvent}" var="event">
            <tr>
                <td>${event.title}</td>
                <td>${event.description}</td>
                <td>${event.beginDate}</td>
                <td>${event.address.name}</td>
                <td>
                    <spring:url value="/event/update/${event.id}" var="updateURL" />
                    <a href="${ updateURL }"> Update </a>
                </td>
                <td>
                    <spring:url value="/event/delete/${event.id}" var="deleteURL" />
                    <a href="${ deleteURL }"> Delete </a>
                </td>
                 <td>
                    <spring:url value="/event/item/${event.id}" var="itemURL" />
                    <a href="${ itemURL }"> Ajout Items </a>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>