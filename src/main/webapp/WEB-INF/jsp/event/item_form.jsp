<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form"  uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/bootstrap.css" rel="stylesheet"/>
<title>Item Form</title>
</head>
<body>
	<h1> Item Form </h1>
	<spring:url value="/event/saveItem" var="saveURL"></spring:url>
	<form:form modelAttribute="item" method="POST" action="${saveURL }">
		<form:hidden path="event.id" />
		<form:hidden path="id" />
		<table>
			<tr>
				<td>Nom</td>
				<td><form:input path="Name" /></td>
			</tr>
			<tr>
				<td>Quantité</td>
				<td><form:input path="neededQuantity" /></td>
			</tr>
			<tr>
				<td>Restant</td>
				<td><form:input path="currentQUantity" /></td>
			</tr>

			
			<tr>
				<td></td>
				<td><button type="submit">Add</button></td>
			</tr>
		</table>
	</form:form>
	
	

	
</body>
</html>
