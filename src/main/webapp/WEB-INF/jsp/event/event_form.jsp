<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form"  uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/bootstrap.css" rel="stylesheet"/>
<title>Event Form</title>
</head>
<body>
	<h1> Event Form </h1>
	<spring:url value="/event/save" var="saveURL"></spring:url>
	<form:form modelAttribute="event" method="POST" action="${saveURL }">
		<form:hidden path="id" />
		<table>
			<tr>
				<td>Nom de l'événement</td>
				<td><form:input path="title" /></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><form:input path="description" /></td>
			</tr>
			<tr>
				<td>Date de début</td>
				<td><form:input path="beginDate" /></td>
			</tr>
			<tr>
			<form:form modelAttribute="adresse" method="POST" action="${saveURL }">
					<form:hidden path="id" />
						<tr>
							<td>Nom de la rue</td>
							<td><form:input path="name" /></td>
						</tr>
						<tr>
							<td>numéro</td>
							<td><form:input path="street" /></td>
						</tr>
						<tr>
							<td>commentaire</td>
							<td><form:input path="comments" /></td>
						</tr>
						<tr>
							<td>Code Postal</td>
							<td><form:input path="zipCode" /></td>
						</tr>
						<tr>
							<td>Ville</td>
							<td><form:input path="city" /></td>
						</tr>
			</form:form>
			</tr>

			<tr>
				<td></td>
				<td><button type="submit">Save</button></td>
			</tr>
		</table>
	</form:form>
	
</body>
</html>
