<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<title>User page</title>
</head>
<body>
    <spring:url value="/user/add" var="addURL" />
    <a href="${addURL}">Add User</a>
    
    <h1> user list </h1>
    <table width="100%" border="1">
        <tr>
            <th>ID</th>
            <th>Login</th>
            <th>Password </th>
            <th>Email</th>
        </tr>
        <c:forEach items="${listUser}" var="user">
            <tr>
                <td>${user.id}</td>
                <td>${user.login}</td>
                <td>${user.pass}</td>
                <td>${user.email}</td>
                <td>
                    <spring:url value="/user/update/${user.id}" var="updateURL" />
                    <a href="${ updateURL }"> Update </a>
                </td>
                <td>
                    <spring:url value="/user/delete/${user.id}" var="deleteURL" />
                    <a href="${ deleteURL }"> Delete </a>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>