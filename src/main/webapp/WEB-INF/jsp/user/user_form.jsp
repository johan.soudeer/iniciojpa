<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form"  uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring"  uri="http://www.springframework.org/tags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="../css/bootstrap.css" rel="stylesheet"/>
<title>User Form</title>
</head>
<body>
	<h1> User Form </h1>
	<spring:url value="/user/save" var="saveURL"></spring:url>
	<form:form modelAttribute="userForm" method="POST" action="${saveURL }">
		<form:hidden path="id" />
		<table>
			<tr>
				<td>Login</td>
				<td><form:input path="login" /></td>
			</tr>
			<tr>
				<td>Pass</td>
				<td><form:input path="pass" /></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><form:input path="email" /></td>
			</tr>
			<tr>
				<td></td>
				<td><button type="submit">Save</button></td>
			</tr>
		</table>
	</form:form>
	
</body>
</html>
